FROM alpine:3.15.4

WORKDIR /app

COPY hello.sh .

RUN chmod +x hello.sh

CMD ./hello.sh